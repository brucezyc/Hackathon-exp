﻿using O2DESNet.Warehouse.Statics;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;

namespace O2DESNet.Warehouse.Dynamics
{
    [Serializable]
    internal class Status
    {
        private Simulator _sim;
        /// <summary>
        /// Simulator Start Time
        /// </summary>
        public DateTime StartTime { get; private set; }

        #region Order Picking and Picklist
        public Dictionary<PickerType, int> TotalPickJobsCompleted { get; private set; }
        public Dictionary<PickerType, int> TotalPickListsCompleted { get; private set; }
        public Dictionary<PickerType, int> MaxPickListSize { get; private set; }
        public Dictionary<PickerType, int> MinPickListSize { get; private set; }
        public Dictionary<PickerType, TimeSpan> TotalPickingTime { get; private set; }
        

        public Dictionary<PickerType, double> MinCartUtilisation { get; private set; }
        public Dictionary<PickerType, double> MaxCartUtilisation { get; private set; }
        public Dictionary<PickerType, List<double>> AllCartUtilisation { get; private set; }

        public TimeSpan TotalPickListWaitingTime { get; set; }
        
        public int NumActivePickers { get; private set; }
        public int MaxActivePickers { get; private set; }
        public TimeSpan AreaPickerTime { get; private set; }
        public DateTime NumPickersJumpTime { get; private set; }

        public double GetAverageNumActivePickers()
        {
            var duration = NumPickersJumpTime - StartTime;

            return AreaPickerTime.Ticks / duration.Ticks;
        }

        private void AccrueAreaPickerTime()
        {
            AreaPickerTime += MultiplyTimeSpan(_sim.ClockTime - NumPickersJumpTime, NumActivePickers);
            NumPickersJumpTime = _sim.ClockTime;
        }

        public void IncrementActivePicker()
        {
            AccrueAreaPickerTime();

            NumActivePickers++;
            if (NumActivePickers > MaxActivePickers) MaxActivePickers = NumActivePickers;
        }
        public void DecrementActivePicker()
        {
            AccrueAreaPickerTime();

            NumActivePickers--;
        }

        public TimeSpan GetAveragePickListTime(PickerType type)
        {
            if (TotalPickListsCompleted[type] == 0) return TimeSpan.Zero;

            return TimeSpan.FromSeconds(TotalPickingTime[type].TotalSeconds / TotalPickListsCompleted[type]);
        }
        #endregion

        public List<int> OrderBatchesTotesCount { get; set; }
        public int NumItemsSorted { get; set; }
        public int MaxNumItemsSorted { get; set; }



        internal Status(Simulator simulation)
        {
            _sim = simulation;

            TotalPickJobsCompleted = new Dictionary<PickerType, int>();
            TotalPickListsCompleted = new Dictionary<PickerType, int>();
            TotalPickingTime = new Dictionary<PickerType, TimeSpan>();
            MaxPickListSize = new Dictionary<PickerType, int>();
            MinPickListSize = new Dictionary<PickerType, int>();
            MaxCartUtilisation = new Dictionary<PickerType, double>();
            MinCartUtilisation = new Dictionary<PickerType, double>();
            AllCartUtilisation = new Dictionary<PickerType, List<double>>();

            TotalPickListWaitingTime = TimeSpan.Zero;

            NumActivePickers = 0;
            MaxActivePickers = 0;
            AreaPickerTime = TimeSpan.Zero;
            NumPickersJumpTime = _sim.ClockTime;
            StartTime = _sim.ClockTime;

            foreach (var type in _sim.Scenario.NumPickers)
            {
                TotalPickJobsCompleted.Add(type.Key, 0);
                TotalPickListsCompleted.Add(type.Key, 0);
                MaxPickListSize.Add(type.Key, 0);
                MinPickListSize.Add(type.Key, int.MaxValue);
                TotalPickingTime.Add(type.Key, TimeSpan.Zero);

                MaxCartUtilisation.Add(type.Key, 0.0);
                MinCartUtilisation.Add(type.Key, double.PositiveInfinity);
                AllCartUtilisation.Add(type.Key, new List<double>());
            }
        }

        private TimeSpan MultiplyTimeSpan(TimeSpan duration, int multiplier)
        {
            duration = TimeSpan.FromTicks(duration.Ticks * multiplier);
            return duration;
        }
    }
}
