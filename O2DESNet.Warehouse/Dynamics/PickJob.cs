﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using O2DESNet.Warehouse.Statics;

namespace O2DESNet.Warehouse.Dynamics
{
    [Serializable]
    public class PickJob
    {

        public PathShelf shelf { get; set; }
        public int quantity { get; set; }

        // HACK: qty is always 1
        public PickJob(PathShelf loc, int qty = 1)
        {            
            shelf = loc; quantity = qty;
        }
    }
}
