﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using O2DESNet.Warehouse.Statics;

namespace O2DESNet.Warehouse.Dynamics
{
    [Serializable]
    public class Picker
    {
        public ControlPoint CurLocation { get; set; }
        public PickerType Type { get; private set; }
        public PickList Picklist { get; set; }
        public List<PickJob> PickJobsToComplete { get; set; }
        public List<PickJob> CompletedJobs { get; set; }
        public List<PickJob> BufferCapacity { get; set; }
        public DateTime StartTime { get; set; }
        public DateTime EndTime { get; set; }
        public bool IsIdle { get; set; }

        public Picker(PickerType type)
        {
            CurLocation = null;
            Type = type;
            IsIdle = true;
            PickJobsToComplete = new List<PickJob>();
            CompletedJobs = new List<PickJob>();
            BufferCapacity = new List<PickJob>();
        }

        // All time in seconds

        /// <summary>
        /// Generic. Does not exploit any warehouse structure.
        /// </summary>
        /// <param name="destination"></param>
        /// <returns></returns>
        public TimeSpan GetTravelTime(ControlPoint destination)
        {
            return TimeSpan.FromSeconds(Type.GetNextTravelTime(CurLocation, destination));
        }

        public TimeSpan GetPickingTime()
        {
            return Type.GetNextPickingTime();
        }
        public void PickItem()
        {
            var pickJob = PickJobsToComplete.First();
            if (CurLocation != pickJob.shelf.BaseCP) throw new Exception("ERROR! Wrong location, halt pick");

            //pickJob.item.PickFromRack(pickJob.rack, pickJob.quantity);

            CompletedJobs.Add(pickJob);
            BufferCapacity.Add(pickJob);

            PickJobsToComplete.RemoveAt(0);
        }

        public int GetNumCompletedPickJobs()
        {
            return CompletedJobs.Count;
        }
        public TimeSpan GetTimeToCompletePickList()
        {
            if (EndTime < StartTime) throw new Exception("Error: EndTime < StartTime");

            return EndTime - StartTime;
        }

        public void SortPickList()
        {
            var newJobsToComplete = from element in PickJobsToComplete
                                    orderby CurLocation.GetDistanceTo(element.shelf.BaseCP)
                                    select element;
            PickJobsToComplete = newJobsToComplete.ToList();
        }

    }
}
