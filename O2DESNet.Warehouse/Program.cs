﻿using O2DESNet.Warehouse.Statics;
using System;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace O2DESNet.Warehouse
{
    class Program
    {
        static void Main(string[] args)
        {
            //WarehouseSim whsim_base = new WarehouseSim("ZA");
            //var byteArray = Serializer.ObjectToByteArray(whsim_base);
            //WarehouseSim whsim = (WarehouseSim) Serializer.ByteArrayToObject(byteArray);
            //whsim.GeneratePicklist(strategy);

            ExperimentRun();

            //ExperimentSelectStrategy();

            Console.WriteLine("\n:: Experiment End ::");
            Console.WriteLine("\nEnter any key to exit...");
            Console.ReadKey();
        }

        private static void ExperimentRun()
        {
            WarehouseSim whsim = null;

            int NumRuns = 1;
            int NumStrategies = 3;

            IOHelper.ClearOutputFiles("Hackathon");

            for (int runID = 1; runID <= NumRuns; runID++)
            {
                for (int strategy = 0; strategy < NumStrategies; strategy++)
                {
                    whsim = null;
                    whsim = new WarehouseSim("Hackathon", strategy, runID);

                    int i = 1;
                    whsim.Run(24);
                    if (strategy == 0)
                    {
                        Console.WriteLine("Total pickjobs completed by strategy {0}: {1}", strategy, whsim.wh.AllPickers[0].CompletedJobs.Count);
                    }
                    else 
                    {
                        Console.WriteLine("Total pickjobs completed by strategy {0}: {1}", strategy, whsim.wh.AllRobots.Sum(s => s.CompletedJobs.Count));
                    }


                    whsim = null;
                }

            }
        }


    }
}
