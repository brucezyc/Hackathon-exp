﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;
using O2DESNet.Warehouse.Statics;
using O2DESNet.Warehouse.Dynamics;

namespace O2DESNet.Warehouse
{
    [Serializable]
    public class WarehouseSim
    {
        public Simulator sim { get; private set; }
        public Scenario wh { get; private set; }


        public int RunID { get; private set; }

        public WarehouseSim(string scenarioName, int strategy,  int runID = 1)
        {
            RunID = runID;
            IOHelper.ReadInputParams(scenarioName, RunID);
            
            InitializeScenario(scenarioName, strategy);
            sim = new Simulator(wh); // Only after warehouse has been built and initialised properly.
        }


        private void InitializeScenario(string scenarioName, int strategy)
        {
            wh = new Scenario(scenarioName);
            
            wh.Strategy = strategy;

            HackathonBuilder(wh);

            wh.ReadParams();

            wh.InitializeRouting();


        }

        private void HackathonBuilder(Scenario scenario)
        {
            // Dimensions in metres

            string[] zone = { "A", "B" };
            int numPairs = zone.Count() / 2;
            int numRows = 20; //160
            int numShelves = 20; //20
            int numRacks = 9; //9
            double interRowSpace = 1.7;
            double shelfWidth = 1.5;
            double rackHeight = 0.35;

            double aisleLength = numRows * interRowSpace;
            double rowLength = numShelves * shelfWidth;
            double shelfHeight = numRacks * rackHeight;
            double interAisleDist = rowLength;


            var mainAisle = scenario.CreateAisle("MAIN", numPairs * rowLength);

            // rowPair
            for (int z = 0; z < numPairs; z++)
            {
                var sideAisle1 = scenario.CreateAisle(zone[2 * z] + zone[2 * z + 1] + "1", aisleLength);
                var sideAisle2 = scenario.CreateAisle(zone[2 * z] + zone[2 * z + 1] + "2", aisleLength);


                scenario.Connect(mainAisle, sideAisle1, 0, 0);
                scenario.Connect(mainAisle, sideAisle2);

                // Rows
                for (int i = 1; i <= numRows; i++)
                {
                    var row1 = scenario.CreateRow(zone[2 * z] + "-" + i.ToString(), rowLength, sideAisle1, i * interRowSpace, sideAisle2, i * interRowSpace);

                    // Shelves
                    for (int j = 1; j <= numShelves; j++)
                    {
                        var shelf1 = scenario.CreateShelf(row1.Row_ID + "-" + j.ToString(), shelfWidth, shelfHeight, row1, j * shelfWidth);

                        // Racks
                        for (int k = 1; k <= numRacks; k++)
                        {
                            scenario.CreateRack(shelf1.Shelf_ID + "-" + k.ToString(), shelf1, k * rackHeight);
                        }
                    }
                }
            }

            scenario.StartCP = mainAisle.ControlPoints[0];

        }



        public void Run(double hours)
        {
            sim.Run(TimeSpan.FromHours(hours));
        }


        public void OutputRacks()
        {
            using (StreamWriter sw = new StreamWriter(@"Layout\" + wh.Name + "_PhysicalLayout.csv"))
            {
                foreach (var rack in wh.Racks.Keys)
                {
                    sw.WriteLine(rack);
                }
            }
        }



    }
}
