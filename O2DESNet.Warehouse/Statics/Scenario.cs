﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.IO;
using System.Text;
using System.Threading.Tasks;
using O2DESNet.Warehouse.Dynamics;
using O2DESNet.Warehouse.DijkstraSP;

namespace O2DESNet.Warehouse.Statics
{
    [Serializable]
    public class Scenario
    {
        public string Name { get; private set; }

        #region Parent Classes for Dijkstra
        public List<Path> Paths { get; private set; } // Exludes PathShelf & PathRow
        public List<ControlPoint> ControlPoints { get; private set; } // Excludes CPRack & ShelfCP
        #endregion

        #region Pickers
        /// <summary>
        /// PickerType from picker type id string
        /// </summary>
        public Dictionary<string, PickerType> GetPickerType { get; private set; }

        /// <summary>
        /// Numbers of pickers of each type
        /// </summary>
        public Dictionary<PickerType, int> NumPickers { get; private set; }
        public List<Picker> AllPickers { get; private set; }
        #endregion


        #region Robots
        /// <summary>
        /// PickerType from picker type id string
        /// </summary>
        public Dictionary<string, RobotType> GetRobotType { get; private set; }

        /// <summary>
        /// Numbers of pickers of each type
        /// </summary>
        public Dictionary<RobotType, int> NumRobots { get; private set; }
        public List<Robot> AllRobots { get; private set; }
        #endregion

        public int Strategy { get; set; }

        #region Layout Lookup - from ID to object
        public Dictionary<string, PathAisle> Aisles { get; private set; }
        public Dictionary<string, PathRow> Rows { get; private set; }
        public Dictionary<string, PathShelf> Shelves { get; private set; }
        public Dictionary<string, CPRack> Racks { get; private set; }
        public ControlPoint StartCP { get; set; }
        #endregion

        public List<PathShelf> ShelvesList { get; private set; }

        public Scenario(string name)
        {
            ControlPoint._count = 0;
            Statics.Path._count = 0;

            Name = name;
            Paths = new List<Path>();
            ControlPoints = new List<ControlPoint>();

            GetPickerType = new Dictionary<string, PickerType>();
            NumPickers = new Dictionary<PickerType, int>();

            GetRobotType = new Dictionary<string, RobotType>();
            NumRobots = new Dictionary<RobotType, int>();

            Aisles = new Dictionary<string, PathAisle>();
            Rows = new Dictionary<string, PathRow>();
            Shelves = new Dictionary<string, PathShelf>();
            Racks = new Dictionary<string, CPRack>();

            ShelvesList = new List<PathShelf>();
            
            StartCP = null;

        }

        #region Picker Reader
        public void ReadParams()
        {

            PickerType type = new PickerType("1", IOHelper.PickerMoveSpeed, TimeSpan.FromSeconds(IOHelper.PickerPickSpeed), IOHelper.PickerCapacity);
            GetPickerType.Add("1", type);
            NumPickers.Add(type, 1);
            AllPickers = NumPickers.SelectMany(item => Enumerable.Range(0, item.Value).Select(i => new Picker(item.Key))).ToList();
            
            RobotType type1 = new RobotType("1", IOHelper.RobotMoveSpeed, TimeSpan.FromSeconds(IOHelper.RobotPickSpeed), TimeSpan.FromSeconds(IOHelper.RobotUnloadTravelTime), IOHelper.RobotCapacity);
            GetRobotType.Add("1", type1);
            NumRobots.Add(type1, 3);
            AllRobots = NumRobots.SelectMany(item => Enumerable.Range(0, item.Value).Select(i => new Robot(item.Key))).ToList();
        }
        #endregion

        #region View Layout
        public void ViewAll()
        {
            ViewAisles();
            ViewRows();
            ViewShelves();
            ViewRacks();
        }
        public void ViewAisles()
        {
            Console.WriteLine("--------------------------------------");
            Console.WriteLine("                Aisles                ");
            Console.WriteLine("--------------------------------------");

            foreach (var a in Aisles.Values.ToList())
            {
                Console.WriteLine("Aisle {0}\tLength {1}", a.Aisle_ID, a.Length);
                Console.WriteLine("Row_ID\tLength");
                foreach (var r in a.Rows)
                {
                    Console.WriteLine("{0}\t{1}", r.Row_ID, r.Length);
                }

            }
        }
        public void ViewRows()
        {
            Console.WriteLine("--------------------------------------");
            Console.WriteLine("                 Rows                 ");
            Console.WriteLine("--------------------------------------");

            foreach (var r in Rows.Values.ToList())
            {
                Console.WriteLine("Row {0}\tLength {1}", r.Row_ID, r.Length);
                Console.WriteLine("In {0}\tOut {1}", r.AisleIn.Aisle_ID, ((r.AisleOut != null) ? r.AisleOut.Aisle_ID : "NIL"));
                Console.WriteLine("Shelf_ID\tHeight");
                foreach (var s in r.Shelves)
                {
                    Console.WriteLine("{0}\t{1}", s.Shelf_ID, s.Length);
                }

            }
        }
        public void ViewShelves()
        {
            Console.WriteLine("--------------------------------------");
            Console.WriteLine("                Shelves               ");
            Console.WriteLine("--------------------------------------");

            foreach (var s in Shelves.Values.ToList())
            {
                Console.WriteLine("Shelf {0}\tHeight {1}\tOnRow {2}", s.Shelf_ID, s.Length, s.Row.Row_ID);
                foreach (var r in s.Racks)
                {
                    Console.WriteLine("{0}", r.Rack_ID);
                }

            }
        }
        public void ViewRacks()
        {
            Console.WriteLine("--------------------------------------");
            Console.WriteLine("                 Racks                ");
            Console.WriteLine("--------------------------------------");

            foreach (var r in Racks.Values.ToList())
            {
                Console.WriteLine("Rack {0}", r.Rack_ID);
            }
        }
        #endregion

        #region Layout Builder Functions
        /// <summary>
        /// Create and return a new aisle
        /// </summary>
        public PathAisle CreateAisle(string aisle_ID, double length, double maxSpeed = double.PositiveInfinity, Direction direction = Direction.TwoWay)
        {
            var aisle = new PathAisle(aisle_ID, length, maxSpeed, direction);
            Paths.Add(aisle);
            Aisles.Add(aisle_ID, aisle);
            return aisle;
        }
        /// <summary>
        /// Create and return a new row, connected to aisle(s)
        /// </summary>
        public PathRow CreateRow(string row_ID, double length, PathAisle aisleIn, double inPos, PathAisle aisleOut = null, double outPos = double.NegativeInfinity,
            double maxSpeed = double.PositiveInfinity, Direction direction = Direction.TwoWay)
        {
            var row = new PathRow(row_ID, length, aisleIn, aisleOut, maxSpeed, direction);
            Paths.Add(row); // Exclude PathRow from Dijkstra : for performance
            Rows.Add(row_ID, row);
            Connect(row, aisleIn, 0, inPos);
            row.BaseCP = row.ControlPoints[0];
            if (aisleOut != null)
                if (!double.IsNegativeInfinity(outPos))
                    Connect(row, aisleOut, row.Length, outPos);
                else
                    throw new Exception("Specify aisleOut position");

            return row;
        }
        /// <summary>
        /// Create and return a new shelf, connected to a row
        /// </summary>
        public PathShelf CreateShelf(string shelf_ID, double width, double height, PathRow row, double pos,
            double maxSpeed = double.PositiveInfinity, Direction direction = Direction.TwoWay)
        {
            var shelf = new PathShelf(shelf_ID, height, row, maxSpeed, direction);
            //Paths.Add(shelf); //Exclude Shelf from Dijkstra : for performance
            Shelves.Add(shelf_ID, shelf);
            ShelvesList.Add(shelf);
            Connect(shelf, row, 0, pos - width / 2);
            shelf.BaseCP = shelf.ControlPoints[0];

            //ControlPoints.Remove(shelf.BaseCP); // Exclude shelfCP from Dijkstra : for performance
            //ControlPoint._count--; // Decrement _count

            return shelf;
        }

        /// <summary>
        /// Create and return a new rack, on a shelf. Optional SKUs on rack.
        /// </summary>
        public CPRack CreateRack(string rack_ID, PathShelf shelf, double position)
        {
            var rack = new CPRack(rack_ID, shelf);
            shelf.Add(rack, position);
            //ControlPoints.Add(rack); // Exclude CPRack from Dijkstra : for performance
            Racks.Add(rack_ID, rack);

            return rack;
        }

        public void AddPickers(PickerType pickerType, int quantity)
        {
            if (!NumPickers.ContainsKey(pickerType)) NumPickers.Add(pickerType, 0);
            NumPickers[pickerType] += quantity;
        }

        // Consider making private the 3 methods below:
        /// <summary>
        /// Create and return a new control point
        /// </summary>
        public ControlPoint CreateControlPoint(Path path, double position)
        {
            var controlPoint = new ControlPoint();
            path.Add(controlPoint, position);
            ControlPoints.Add(controlPoint);
            return controlPoint;
        }
        /// <summary>
        /// Connect two paths at specified positions
        /// </summary>
        public void Connect(Path path_0, Path path_1, double position_0, double position_1)
        {
            var controlPoint = CreateControlPoint(path_0, position_0);
            path_1.Add(controlPoint, position_1);
        }
        /// <summary>
        /// Connect the end of path_0 to the start of path_1
        /// </summary>
        public void Connect(Path path_0, Path path_1) { Connect(path_0, path_1, path_0.Length, 0); }
        #endregion


        #region For Static Routing (Distance-Based), Using Dijkstra
        public void InitializeRouting()
        {
            ConstructDijkstraSP();

        }



        private List<Dijkstra.Edge> GetEdges(Path path)
        {
            var edges = new List<Dijkstra.Edge>();
            for (int i = 0; i < path.ControlPoints.Count - 1; i++)
            {
                var length = path.ControlPoints[i + 1].Positions[path] - path.ControlPoints[i].Positions[path];
                var from = path.ControlPoints[i].Id;
                var to = path.ControlPoints[i + 1].Id;
                if (path.Direction != Direction.Backward) edges.Add(new Dijkstra.Edge(from, to, length));
                if (path.Direction != Direction.Forward) edges.Add(new Dijkstra.Edge(to, from, length));
            }
            return edges;
        }

        private void ConstructDijkstraSP()
        {
            var graph = CreateGraph();
            foreach (var cp in ControlPoints)
            {
                cp.InitShortestPath(graph);
            }
        }
        private EdgeWeightedDigraph CreateGraph()
        {
            EdgeWeightedDigraph Graph = new EdgeWeightedDigraph(ControlPoints.Count + 1);

            var edges = Paths.SelectMany(path => GetDirectedEdges(path)).ToList();

            foreach (var edge in edges)
            {
                Graph.AddEdge(edge);
            }

            return Graph;
        }
        private List<DirectedEdge> GetDirectedEdges(Path path)
        {
            var edges = new List<DirectedEdge>();

            // For each control point in path
            for (int i = 0; i < path.ControlPoints.Count - 1; i++)
            {
                for (int j = 0; j < path.ControlPoints.Count; j++)
                {
                    if (j>i)
                    {
                        var length = path.ControlPoints[j].Positions[path] - path.ControlPoints[i].Positions[path];
                        var from = path.ControlPoints[i].Id;
                        var to = path.ControlPoints[j].Id;
                        if (path.Direction != Direction.Backward) edges.Add(new DirectedEdge(from, to, length));
                        if (path.Direction != Direction.Forward) edges.Add(new DirectedEdge(to, from, length));
                    }
                }
                
            }

            return edges;
        }
        #endregion
    }
}
