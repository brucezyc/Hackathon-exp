﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;
using O2DESNet.Warehouse.Dynamics;
using MathNet.Numerics.Distributions;
using MathNet.Numerics.Random;

namespace O2DESNet.Warehouse.Statics
{
    [Serializable]
    /// <summary>
    ///  class for generating picklists based on specified rule.
    /// Requires the use of Layout, SKU and inventory snapshot.
    /// </summary>
    public class PicklistGenerator
    {
        public PickList OriginalPicklist { get; set; }
        public PickList SortedPicklist { get; set; }
        public int AvgNumItems { get; set; }
        public Scenario PGScenario { get; set; }

        public PicklistGenerator(Scenario scenario)
        {
            OriginalPicklist = new PickList();
            SortedPicklist = new PickList();
            PGScenario = scenario;
            
        }


        public PickList GeneratePickList(int avgNumItems)
        {
            var u_PickListLength = new DiscreteUniform(1, 2 * avgNumItems);
            int pickListLength = u_PickListLength.Sample();

            for (int i = 0; i < pickListLength; i++)
            {
                var u_shelfNumber = new DiscreteUniform(1, PGScenario.ShelvesList.Count());
                var shelf = PGScenario.ShelvesList[u_shelfNumber.Sample() - 1];
                OriginalPicklist.Add(new PickJob(shelf));
            }

            SortedPicklist = SortByLocation(OriginalPicklist, PGScenario);

            return SortedPicklist;
        }


        private PickList SortByLocation(PickList origList, Scenario scenario)
        {
            var temp = new PickList();

            var newJobsToComplete = from element in origList.pickJobs
                                    orderby scenario.StartCP.GetDistanceTo(element.shelf.BaseCP)
                                    select element;
            temp.pickJobs = newJobsToComplete.ToList();

            return temp;

        }

    }
}
