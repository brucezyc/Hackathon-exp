﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using O2DESNet.Warehouse.Dynamics;

namespace O2DESNet.Warehouse.Events
{
    [Serializable]
    internal class RobotEndPick : Event
    {
        internal Robot robot { get; private set; }

        internal RobotEndPick(Simulator sim, Robot robot) : base(sim)
        {
            this.robot = robot;
        }
        public override void Invoke()
        {
            // Check
            if (robot.PickJobsToComplete.Count > 0) throw new Exception("There are still items to pick!");

            // Just status update
            //picker.Picklist.endPickTime = _sim.ClockTime;
            robot.CurLocation = _sim.Scenario.StartCP;
            robot.EndTime = _sim.ClockTime;
            robot.IsIdle = true;
            _sim.Status.DecrementActivePicker();

        }

        public override void Backtrack()
        {
            throw new NotImplementedException();
        }
    }
}
