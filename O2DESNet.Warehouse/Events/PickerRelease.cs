﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using O2DESNet.Warehouse.Dynamics;

namespace O2DESNet.Warehouse.Events
{
    [Serializable]
    internal class PickerRelease : Event
    {
        internal Picker picker { get; private set; }

        internal PickerRelease(Simulator sim, Picker picker)
            : base(sim)
        {
            this.picker = picker;
        }
        public override void Invoke()
        {
            picker.CurLocation = _sim.Scenario.StartCP;
            picker.BufferCapacity.Clear();


            if (picker.PickJobsToComplete.Count > 0)
            {
                var shelfCP = picker.PickJobsToComplete.First().shelf.BaseCP;
                var duration = picker.GetTravelTime(shelfCP) + TimeSpan.FromSeconds(IOHelper.RobotUnloadTravelTime); // robot has unloading time
                _sim.ScheduleEvent(new PickerArriveLocation(_sim, picker), _sim.ClockTime.Add(duration));
            }
            else
            {
                _sim.ScheduleEvent(new PickerEndPick(_sim, picker), _sim.ClockTime);
            }
        }

        public override void Backtrack()
        {
            throw new NotImplementedException();
        }
    }
}
