﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using O2DESNet.Warehouse.Dynamics;
using MathNet.Numerics.Distributions;
using MathNet.Numerics.Random;

namespace O2DESNet.Warehouse.Events
{
    [Serializable]
    internal class JobArrive : Event
    {
        public JobArrive(Simulator sim) : base(sim) { }

        public override void Invoke()
        {
            var u1_40 = new DiscreteUniform(1, _sim.Scenario.ShelvesList.Count());
            var shelf = _sim.Scenario.ShelvesList[u1_40.Sample() - 1];
            var newjob = new PickJob(shelf);

            //Console.WriteLine("Job arrives at shelf " + shelf.Shelf_ID);

            if (_sim.Scenario.Strategy == 0)
            {
                _sim.Scenario.AllPickers[0].PickJobsToComplete.Add(newjob);
                if (_sim.Scenario.AllPickers[0].IsIdle)
                {
                    _sim.Scenario.AllPickers[0].IsIdle = false;
                    _sim.ScheduleEvent(new PickerStartPick(_sim, _sim.Scenario.AllPickers[0]),_sim.ClockTime);
                }

            }
            else if (_sim.Scenario.Strategy == 1)
            {
                _sim.Scenario.AllRobots[0].PickJobsToComplete.Add(newjob);
                if (_sim.Scenario.AllRobots[0].IsIdle)
                {
                    _sim.Scenario.AllRobots[0].IsIdle = false;
                    _sim.ScheduleEvent(new RobotStartPick(_sim, _sim.Scenario.AllRobots[0]), _sim.ClockTime);
                }

            }



            _sim.ScheduleEvent(new JobArrive(_sim), _sim.ClockTime + TimeSpan.FromSeconds(IOHelper.JobInterval));
        }

        public override void Backtrack()
        {
            throw new NotImplementedException("Unable to Backtrack Start Simulation Event");
        }
    }
}
