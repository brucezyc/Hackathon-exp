﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using O2DESNet.Warehouse.Dynamics;

namespace O2DESNet.Warehouse.Events
{
    [Serializable]
    internal class StartSim : Event
    {
        public StartSim(Simulator sim) : base(sim) { }

        public override void Invoke()
        {


            foreach (var picker in _sim.Scenario.AllPickers)
            {
                picker.CurLocation = _sim.Scenario.StartCP;
            }

            foreach (var robot in _sim.Scenario.AllRobots)
            {
                robot.CurLocation = _sim.Scenario.StartCP;
            }

            if (_sim.Scenario.Strategy == 2)
            {
                _sim.ScheduleEvent(new PickListArrive(_sim), _sim.ClockTime);
            }
            else
            {
                _sim.ScheduleEvent(new JobArrive(_sim), _sim.ClockTime);
            }
        }

        public override void Backtrack()
        {
            throw new NotImplementedException("Unable to Backtrack Start Simulation Event");
        }
    }
}
