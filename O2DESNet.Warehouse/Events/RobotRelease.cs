﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using O2DESNet.Warehouse.Dynamics;

namespace O2DESNet.Warehouse.Events
{
    [Serializable]
    internal class RobotRelease : Event
    {
        internal Robot robot { get; private set; }

        internal RobotRelease(Simulator sim, Robot robot)
            : base(sim)
        {
            this.robot = robot;
        }
        public override void Invoke()
        {
            robot.CurLocation = _sim.Scenario.StartCP;
            robot.BufferCapacity.Clear();


            if (robot.PickJobsToComplete.Count > 0)
            {
                var shelfCP = robot.PickJobsToComplete.First().shelf.BaseCP;
                var duration = robot.GetTravelTime(shelfCP); // no unloading time
                _sim.ScheduleEvent(new RobotArriveLocation(_sim, robot), _sim.ClockTime.Add(duration));
            }
            else if(robot.ListOfPickList.Count()>0)
            {
                robot.PickJobsToComplete = robot.ListOfPickList.First().pickJobs.ToList();
                robot.ListOfPickList.RemoveAt(0);
                _sim.ScheduleEvent(new RobotStartPick(_sim, robot), _sim.ClockTime);
            }
            else
            {
                _sim.ScheduleEvent(new RobotEndPick(_sim, robot), _sim.ClockTime);
            }
        }

        public override void Backtrack()
        {
            throw new NotImplementedException();
        }
    }
}
