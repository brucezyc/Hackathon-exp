﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using O2DESNet.Warehouse.Dynamics;
using MathNet.Numerics.Distributions;
using MathNet.Numerics.Random;

namespace O2DESNet.Warehouse.Events
{
    [Serializable]
    internal class PickListArrive : Event
    {
        public PickListArrive(Simulator sim) : base(sim) { }

        public override void Invoke()
        {
            var newPickList = _sim.generator.GeneratePickList(20);
            Robot assignedRobot;

            //Console.WriteLine("Job arrives at shelf " + shelf.Shelf_ID);



            var RobotIdle = _sim.Scenario.AllRobots.Where(s => s.IsIdle);

            if (RobotIdle.Count() != 0)
            {
                assignedRobot = RobotIdle.First();
            }
            else
            {
                assignedRobot = _sim.Scenario.AllRobots.First(_ => _.ListOfPickList.Count() == _sim.Scenario.AllRobots.Min(s => s.ListOfPickList.Count()));
            }

            assignedRobot.ListOfPickList.Add(newPickList);


            if (assignedRobot.IsIdle)
            {
                assignedRobot.IsIdle = false;
                assignedRobot.PickJobsToComplete = assignedRobot.ListOfPickList.First().pickJobs.ToList();
                assignedRobot.ListOfPickList.RemoveAt(0);
                _sim.ScheduleEvent(new RobotStartPick(_sim, assignedRobot), _sim.ClockTime);
            }



            _sim.ScheduleEvent(new PickListArrive(_sim), _sim.ClockTime + TimeSpan.FromSeconds(60));
        }

        public override void Backtrack()
        {
            throw new NotImplementedException("Unable to Backtrack Start Simulation Event");
        }
    }
}
