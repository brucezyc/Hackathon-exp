﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using O2DESNet.Warehouse.Dynamics;

namespace O2DESNet.Warehouse.Events
{
    [Serializable]
    internal class PickerStartPick : Event
    {
        internal Picker picker { get; private set; }

        internal PickerStartPick(Simulator sim, Picker picker) : base(sim)
        {
            this.picker = picker;
        }
        public override void Invoke()
        {


            // check start location
            //if (picker.CurLocation != _sim.Scenario.StartCP) throw new Exception("Picker not at StartCP, unable to start picking job");

            picker.StartTime = _sim.ClockTime;
            picker.IsIdle = false;
            //picker.CompletedJobs.Clear();

            if (picker.PickJobsToComplete.Count > 0)
            {
                var shelfCP = picker.PickJobsToComplete.First().shelf.BaseCP;
                var duration = picker.GetTravelTime(shelfCP);
                _sim.ScheduleEvent(new PickerArriveLocation(_sim, picker), _sim.ClockTime.Add(duration));

                // Any status updates?
                _sim.Status.IncrementActivePicker();
            }
            else
            {
                // Picklist empty
                _sim.ScheduleEvent(new PickerEndPick(_sim, picker), _sim.ClockTime);
            }
        }

        public override void Backtrack()
        {
            throw new NotImplementedException();
        }
    }
}
