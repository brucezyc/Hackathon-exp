﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using O2DESNet.Warehouse.Dynamics;

namespace O2DESNet.Warehouse.Events
{
    [Serializable]
    internal class RobotPickItem : Event
    {
        internal Robot robot { get; private set; }

        internal RobotPickItem(Simulator sim, Robot robot) : base(sim)
        {
            this.robot = robot;
        }
        public override void Invoke()
        {
            robot.PickItem();
            if (robot.BufferCapacity.Count >= robot.Type.Capacity || robot.PickJobsToComplete.Count <= 0)
            {
                var duration = robot.GetTravelTime(_sim.Scenario.StartCP);
                _sim.ScheduleEvent(new RobotRelease(_sim, robot), _sim.ClockTime.Add(duration));
            }
            else if (robot.PickJobsToComplete.Count > 0)
            {
                robot.SortPickList();
                var shelfCP = robot.PickJobsToComplete.First().shelf.BaseCP;
                var duration = robot.GetTravelTime(shelfCP);
                _sim.ScheduleEvent(new RobotArriveLocation(_sim, robot), _sim.ClockTime.Add(duration));
            }
        }

        public override void Backtrack()
        {
            throw new NotImplementedException();
        }
    }
}
