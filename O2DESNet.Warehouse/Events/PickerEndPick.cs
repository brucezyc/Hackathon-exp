﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using O2DESNet.Warehouse.Dynamics;

namespace O2DESNet.Warehouse.Events
{
    [Serializable]
    internal class PickerEndPick : Event
    {
        internal Picker picker { get; private set; }

        internal PickerEndPick(Simulator sim, Picker picker) : base(sim)
        {
            this.picker = picker;
        }
        public override void Invoke()
        {
            // Check
            if (picker.PickJobsToComplete.Count > 0) throw new Exception("There are still items to pick!");

            // Just status update
            //picker.Picklist.endPickTime = _sim.ClockTime;
            picker.CurLocation = _sim.Scenario.StartCP;
            picker.EndTime = _sim.ClockTime;
            picker.IsIdle = true;
            _sim.Status.DecrementActivePicker();

        }

        public override void Backtrack()
        {
            throw new NotImplementedException();
        }
    }
}
