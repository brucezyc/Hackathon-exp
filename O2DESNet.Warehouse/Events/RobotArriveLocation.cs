﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using O2DESNet.Warehouse.Dynamics;

namespace O2DESNet.Warehouse.Events
{
    [Serializable]
    internal class RobotArriveLocation : Event
    {
        internal Robot robot { get; private set; }

        internal RobotArriveLocation(Simulator sim, Robot robot) : base(sim)
        {
            this.robot = robot;
        }
        public override void Invoke()
        {
            robot.CurLocation = robot.PickJobsToComplete.First().shelf.BaseCP;
            var duration = robot.GetPickingTime();
            _sim.ScheduleEvent(new RobotPickItem(_sim, robot), _sim.ClockTime.Add(duration));
        }

        public override void Backtrack()
        {
            throw new NotImplementedException();
        }
    }
}
