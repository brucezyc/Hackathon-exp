﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using O2DESNet.Warehouse.Dynamics;

namespace O2DESNet.Warehouse.Events
{
    [Serializable]
    internal class RobotStartPick : Event
    {
        internal Robot robot { get; private set; }

        internal RobotStartPick(Simulator sim, Robot robot) : base(sim)
        {
            this.robot = robot;
        }
        public override void Invoke()
        {


            // check start location
            //if (picker.CurLocation != _sim.Scenario.StartCP) throw new Exception("Picker not at StartCP, unable to start picking job");

            robot.StartTime = _sim.ClockTime;
            robot.IsIdle = false;
            //picker.CompletedJobs.Clear();

            if (robot.PickJobsToComplete.Count > 0)
            {
                var shelfCP = robot.PickJobsToComplete.First().shelf.BaseCP;
                var duration = robot.GetTravelTime(shelfCP);
                _sim.ScheduleEvent(new RobotArriveLocation(_sim, robot), _sim.ClockTime.Add(duration));

                // Any status updates?

            }
            else
            {
                // Picklist empty
                _sim.ScheduleEvent(new RobotEndPick(_sim, robot), _sim.ClockTime);
            }
        }

        public override void Backtrack()
        {
            throw new NotImplementedException();
        }
    }
}
