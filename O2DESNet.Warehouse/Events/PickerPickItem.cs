﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using O2DESNet.Warehouse.Dynamics;

namespace O2DESNet.Warehouse.Events
{
    [Serializable]
    internal class PickerPickItem : Event
    {
        internal Picker picker { get; private set; }

        internal PickerPickItem(Simulator sim, Picker picker) : base(sim)
        {
            this.picker = picker;
        }
        public override void Invoke()
        {
            picker.PickItem();
            if (picker.BufferCapacity.Count >= picker.Type.Capacity || picker.PickJobsToComplete.Count <= 0)
            {
                var duration = picker.GetTravelTime(_sim.Scenario.StartCP);
                _sim.ScheduleEvent(new PickerRelease(_sim, picker), _sim.ClockTime.Add(duration));
            }
            else if (picker.PickJobsToComplete.Count > 0)
            {
                picker.SortPickList();
                var shelfCP = picker.PickJobsToComplete.First().shelf.BaseCP;
                var duration = picker.GetTravelTime(shelfCP);
                _sim.ScheduleEvent(new PickerArriveLocation(_sim, picker), _sim.ClockTime.Add(duration));
            }
        }

        public override void Backtrack()
        {
            throw new NotImplementedException();
        }
    }
}
