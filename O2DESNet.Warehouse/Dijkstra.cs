﻿using System;
using System.Reflection;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.InteropServices;

namespace O2DESNet.Warehouse
{
    internal class Dijkstra
    {


        internal int NumNodes { get; private set; }
        internal Edge[] Edges { get; private set; }
        internal int[] Parents { get; private set; }

        /// <summary>
        /// Constract a shortest path problem with single source at index 0, and solve by Dijkstra algorithm
        /// </summary>
        internal Dijkstra(params Edge[] edges)
        {
            // intialize properties
            Edges = Standardize(edges);
            NumNodes = Math.Max(Edges.Max(e => e.FromIndex), Edges.Max(e => e.ToIndex)) + 1;

            Parents = new int[NumNodes];

        }
        private Edge[] Standardize(Edge[] edges)
        {
            var min = edges.Min(e => e.Distance);
            if (min < 0) return edges.Select(e => new Edge(e.FromIndex, e.ToIndex, e.Distance - min)).ToArray();
            return edges.ToArray();
        }



        internal class Edge
        {
            internal int FromIndex { get; private set; }
            internal int ToIndex { get; private set; }
            internal double Distance { get; private set; }
            internal Edge(int from, int to, double distance) { FromIndex = from; ToIndex = to; Distance = distance; }
        }


    }
}
